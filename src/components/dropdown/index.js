export class Dropdown {
  constructor(element) {
    if (!element) return
    this.$el = element
    this.$head = this.$el.querySelector('[data-ddwn-head]')
    this.$body = this.$el.querySelector('[data-ddwn-body]')
    this.$items = this.$el.querySelectorAll('[data-ddwn-item]')

    this.current = this.$el.dataset['ddwn'] || this.$items[0].dataset['ddwn-item'] || 0
    this.isShow = false

    this.setup()
  }

  setup() {
    this.$el.addEventListener('mouseenter', this.handleHover.bind(this))
    this.$el.addEventListener('mouseleave', this.handleHover.bind(this))
  }

  updateShow() {
    if (this.isShow) {
      this.$el.classList.add('is-show')
    } else {
      this.$el.classList.remove('is-show')
    }
  }

  handleHover(e) {
    this.isShow = e.type === 'mouseenter' ? true : false
    this.updateShow()
  }
}

export function installAllDdwn() {
  Array.prototype.forEach.call(
    document.querySelectorAll('[data-ddwn]'),
    item => new Dropdown(item)
  )
}
