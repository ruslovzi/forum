export function expertSliderAll() {
  window.expertsSliders = []
  Array.prototype.forEach.call(
    document.querySelectorAll('.js-slider-experts'),
    item => {
      const el = new Swiper(item.querySelector('.swiper-container'), {
        loop: false,
        spaceBetween: 16,
        slidesPerView: 4,
        navigation: {
          nextEl: item.querySelector('.slider-nav__item_next'),
          prevEl: item.querySelector('.slider-nav__item_prev'),
        },
      })
      expertsSliders.push(el)
    }
  )
}

export function updateExpertSliderAll() {
  expertsSliders.forEach(item => {
    item.update()
  })
}

