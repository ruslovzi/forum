export class Tabs {
  constructor(element) {
    if (!element) return
    this.$el = element
    this.$head = this.$el.querySelector('[data-tabs-head]')
    this.$body = this.$el.querySelector('[data-tabs-body]')
    this.$tabs = this.$el.querySelectorAll('[data-tabs-tab]')
    this.$panels = this.$el.querySelectorAll('[data-tabs-panel]')

    this.current = this.$el.dataset['tabs'] || this.$tabs[0].dataset['tab'] || 0

    this.setup()
  }

  setup() {
    this.$panels = Array.from(this.$panels).reduce((acc, el) => {
      const id = el.dataset['tabsPanel']
      acc[id] = el
      return acc
    }, {})
    this.$tabs = Array.from(this.$tabs).reduce((acc, el) => {
      const id = el.dataset['tabsTab']
      acc[id] = el
      return acc
    }, {})
    Object.values(this.$tabs).forEach(el => {
      el.addEventListener('click', this.handleHover.bind(this, el))
    })
    this.updateCurrent()
  }

  updateCurrent() {
    Object.values(this.$tabs).forEach(el => el.classList.remove('is-active'))
    Object.values(this.$panels).forEach(el => el.classList.remove('is-active'))
    if (this.$tabs[this.current]) this.$tabs[this.current].classList.add('is-active')
    if (this.$panels[this.current]) this.$panels[this.current].classList.add('is-active')
  }

  handleHover(el, e) {
    const id = el.dataset['tabsTab']
    if (!id) return;
    this.current = id
    this.updateCurrent()
  }
}

export function installAllTabs() {
  Array.prototype.forEach.call(
    document.querySelectorAll('[data-tabs]'),
    item => new Tabs(item)
  )
}
