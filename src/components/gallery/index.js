export function installAllGallery() {
  Array.prototype.forEach.call(
    document.querySelectorAll('.js-lightgallery'),
    item => {
      lightGallery(item)
    }
  )
}
